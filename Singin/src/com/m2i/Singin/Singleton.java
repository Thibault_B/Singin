package com.m2i.Singin;

public class Singleton
{
	private Singleton()
	{
		
	}
	private String nom = "", prenom = "";
	
	 private static Singleton INSTANCE = null;
	 
	 private static Singleton getInstance()
	 {
		 if (getInstance() == null)
		 {
			 INSTANCE = new Singleton();
			 
			 //
		 }
		 
		 return getInstance();
	 }

	public void setNom(String nom)
	{
		this.nom += nom;
	}

	public void setPrenom(String prenom)
	{
		this.prenom += prenom;
	}

	public String toString()
	{
		return prenom + "\n" + nom;
	}
	
	 
}


